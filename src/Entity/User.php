<?php
	
	namespace App\Entity;
	
	use App\Repository\UserRepository;
	use Doctrine\ORM\Mapping as ORM;
	use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
	use Symfony\Component\Security\Core\User\UserInterface;
	
	#[ORM\Entity(repositoryClass: UserRepository::class)]
	class User implements UserInterface, PasswordAuthenticatedUserInterface
	{
		#[ORM\Id]
		#[ORM\GeneratedValue]
		#[ORM\Column]
		private ?int $id = null;
		
		#[ORM\Column(length: 100, unique: false)]
		private string $name;
		
		#[ORM\Column(length: 100, unique: false)]
		private string $surname;
		
		#[ORM\Column(length: 180, unique: true)]
		private string $email;
		
		#[ORM\Column(length: 250, unique: false, nullable: true)]
		private ?string $description = null;
		
		#[ORM\Column]
		private array $roles = [];

		#[ORM\Column]
		private ?string $password = null;
		
		#[ORM\Column(nullable: true)]
		private array $dataPosition = [];
		
		#[ORM\ManyToOne(targetEntity: Positions::class, inversedBy: 'users')]
		private ?Positions $positions = null;
		
		/**
		 * @return int|null
		 */
		public function getId(): ?int
		{
			return $this->id;
		}
		
		/**
		 * @return string
		 */
		public function getEmail(): string
		{
			return $this->email;
		}
		
		/**
		 * @param string $email
		 * @return $this
		 */
		public function setEmail(string $email): static
		{
			$this->email = $email;
			
			return $this;
		}
		
		/**
		 * @return string
		 */
		public function getName(): string
		{
			return $this->name;
		}
		
		/**
		 * @param string $name
		 * @return User
		 */
		public function setName(string $name): static
		{
			$this->name = $name;
			
			return $this;
		}
		
		/**
		 * @return string
		 */
		public function getSurname(): string
		{
			return $this->surname;
		}
		
		/**
		 * @param string $surname
		 * @return User
		 */
		public function setSurname(string $surname): static
		{
			$this->surname = $surname;
			
			return $this;
		}
		
		/**
		 * @return string
		 */
		public function getDescription(): string
		{
			return $this->description;
		}
		
		/**
		 * @param string $description
		 * @return User
		 */
		public function setDescription(string $description): static
		{
			$this->description = $description;
			
			return $this;
		}
		
		/**
		 * A visual identifier that represents this user.
		 *
		 * @see UserInterface
		 */
		public function getUserIdentifier(): string
		{
			return "$this->email";
		}
		
		/**
		 * @see UserInterface
		 */
		public function getRoles(): array
		{
			$roles = $this->roles;
			// guarantee every user at least has ROLE_USER
			$roles[] = 'ROLE_USER';
			
			return array_unique($roles);
		}
		
		public function setRoles(array $roles): static
		{
			$this->roles = $roles;
			
			return $this;
		}
		
		/**
		 * @see PasswordAuthenticatedUserInterface
		 */
		public function getPassword(): string
		{
			return $this->password;
		}
		
		public function setPassword(string $password): static
		{
			$this->password = $password;
			
			return $this;
		}
		
		/**
		 * @see UserInterface
		 */
		public function eraseCredentials(): void
		{
			// If you store any temporary, sensitive data on the user, clear it here
			// $this->plainPassword = null;
		}
		
		/**
		 * @return array
		 */
		public function getDataPosition(): array
		{
			return $this->dataPosition;
		}
		
		/**
		 * @param array $dataPosition
		 * @return $this
		 */
		public function setDataPosition(array $dataPosition): self
		{
			$this->dataPosition = $dataPosition;
			
			return $this;
		}
		
		/**
		 * @return Positions|null
		 */
		public function getPositions(): ?Positions
		{
			return $this->positions;
		}
		
		public function setPositions(Positions $positions): static
		{
			if ($this->positions !== $positions) {
				$this->positions = $positions;
			}
			
			return $this;
		}
		
	}
