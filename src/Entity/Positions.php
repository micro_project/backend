<?php
	
	namespace App\Entity;
	
	use App\Repository\PositionsRepository;
	use Doctrine\Common\Collections\ArrayCollection;
	use Doctrine\Common\Collections\Collection;
	use Doctrine\ORM\Mapping as ORM;
	
	#[ORM\Entity(repositoryClass: PositionsRepository::class)]
	class Positions
	{
		#[ORM\Id]
		#[ORM\GeneratedValue]
		#[ORM\Column]
		private ?int $id = null;
		
		#[ORM\Column(length: 200)]
		private ?string $name = null;
		
		#[ORM\Column(nullable: true)]
		private ?array $subPosition = [];
		
		#[ORM\OneToMany(targetEntity: User::class, mappedBy: 'positions')]
		private Collection $users;
		
		public function __construct()
		{
			$this->users = new ArrayCollection();
		}
		
		public function getId(): ?int
		{
			return $this->id;
		}
		
		public function getName(): ?string
		{
			return $this->name;
		}
		
		public function setName(string $name): static
		{
			$this->name = $name;
			
			return $this;
		}

		public function setSubPosition(?array $subPosition): static
		{
			$this->subPosition = $subPosition;
			
			return $this;
		}
		
	}
