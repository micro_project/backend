<?php
	
	namespace App\Controller;
	
	use App\Repository\PositionsRepository;
  use Exception;
  use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\JsonResponse;
  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\Routing\Attribute\Route;
	use OpenApi\Attributes as OA;
	
	#[Route('/api', name: 'app_position'), OA\Tag(name: 'Position')]
	class PositionsController extends AbstractController
	{
		private PositionsRepository $positionsRepository;
		
		public function __construct(PositionsRepository $positionsRepository)
		{
			$this->positionsRepository = $positionsRepository;
		}
		
		#[Route('/positions', name: 'app_positions_listing', methods: ['GET'])]
		public function index(): JsonResponse
		{
			try {
				$positions = $this->positionsRepository->findAllPositionsNameWithSubPositions();
				
				return $this->json($positions, Response::HTTP_OK);
				
			} catch (Exception $exception) {
				dd($exception->getMessage(), Response::HTTP_BAD_GATEWAY);
			}
		}
	}
