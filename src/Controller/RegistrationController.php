<?php
	
	namespace App\Controller;
	
	use App\DTO\CreateUserDTO;
	use App\DTO\GetUserMailDTO;
	use App\DTO\UpdateUserDTO;
	use App\Entity\User;
	use App\Message\SendEmail;
	use App\Repository\PositionsRepository;
	use App\Repository\UserRepository;
	use Doctrine\ORM\EntityManagerInterface;
	use Nelmio\ApiDocBundle\Annotation\Model;
	use OpenApi\Attributes as OA;
	use Exception;
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\JsonResponse;
  use Symfony\Component\HttpFoundation\Response;
  use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
	use Symfony\Component\Messenger\MessageBusInterface;
	use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
	use Symfony\Component\Routing\Attribute\Route;
	use Symfony\Component\Security\Http\Attribute\IsGranted;
	
	#[Route('/api', name: 'app_registration'), OA\Tag(name: 'Account')]
	class RegistrationController extends AbstractController
	{
		private PositionsRepository $positionsRepository;
		private UserRepository $userRepository;
		private EntityManagerInterface $entityManager;
		private UserPasswordHasherInterface $passwordHasher;
		private MessageBusInterface $messageBus;
		
		public function __construct(MessageBusInterface         $messageBus,
		                            UserPasswordHasherInterface $passwordHasher,
		                            EntityManagerInterface      $entityManager,
		                            UserRepository              $userRepository,
		                            PositionsRepository         $positionsRepository)
		{
			$this->messageBus = $messageBus;
			$this->passwordHasher = $passwordHasher;
			$this->entityManager = $entityManager;
			$this->userRepository = $userRepository;
			$this->positionsRepository = $positionsRepository;
		}
		
		#[Route('/users', methods: ['GET'])]
		public function getAllRecordsWithRoleUser(): JsonResponse
		{
			try {
				$roles = ["ROLE_USER"];

				return $this->json($this->userRepository->findAllUsersByRoles($roles), Response::HTTP_OK);
				
			} catch (Exception $exception) {
				dd($exception->getMessage(), Response::HTTP_BAD_GATEWAY);
			}
		}
		
		#[Route('/register', methods: ['POST']),
			OA\Post(requestBody: new OA\RequestBody(content: new OA\JsonContent(ref: new Model(type: CreateUserDTO::class))))]
		public function register(#[MapRequestPayload(acceptFormat: 'json')] CreateUserDTO $createUserDTO): JsonResponse
		{
			try {
				$position = $this->positionsRepository->findOneBy([
					"name" => $createUserDTO->positionName
				]);
				$password = uniqid(8);
				$user = new User();
				$user->setName($createUserDTO->name);
				$user->setSurname($createUserDTO->surname);
				$user->setEmail($createUserDTO->email);
				$user->setDescription($createUserDTO->description);
				$user->setRoles(['ROLE_USER']);
				$hashedPassword = $this->passwordHasher->hashPassword(
					$user,
					$password
				);
				$user->setDataPosition($createUserDTO->answers);
				$user->setPassword($hashedPassword);
				$user->setPositions($position);
				
				$checkDuplicateEmail = $this->userRepository->findBy([
					"email" => $createUserDTO->email
				]);
				
				if ($checkDuplicateEmail) {
					return $this->json([
						"detail" => "This mail exist in our database you can't duplicate"
					], Response::HTTP_BAD_REQUEST);
				}
				
				$this->entityManager->persist($user);
				$this->entityManager->flush();
				
				$bodyMail = "This is your data to login webiste. <br>Login: {$user->getEmail()}  <br>Password: $password";
				$email = new SendEmail($user->getEmail(), 'Time for Symfony Mailer!', $bodyMail);
				$this->messageBus->dispatch($email);
				
				return $this->json($createUserDTO, Response::HTTP_CREATED);
			} catch (Exception $exception) {
				return $this->json($exception->getMessage(), Response::HTTP_BAD_GATEWAY);
			}
		}
		
		#[Route('/update/{id}', methods: ['PUT']),
			OA\PUT(requestBody: new OA\RequestBody(content: new OA\JsonContent(ref: new Model(type: UpdateUserDTO::class)))),
			IsGranted("ROLE_ADMIN")]
		public function update(#[MapRequestPayload(acceptFormat: 'json')] UpdateUserDTO $updateUserDTO, int $id): JsonResponse
		{
			try {
				$user = $this->userRepository->find($id);
				
				$position = $this->positionsRepository->findOneBy([
					"name" => $updateUserDTO->positionName
				]);
				$userObject = $this->userRepository->find($id);

				$userObject->setName($updateUserDTO->name);
				$userObject->setSurname($updateUserDTO->surname);
				$userObject->setDescription($updateUserDTO->description);
				$userObject->setDataPosition($updateUserDTO->answers);
				$userObject->setPositions($position);

				$this->entityManager->persist($position);
				$this->entityManager->flush();
				
				return $this->json($user, Response::HTTP_CREATED);
				
			} catch (Exception $exception) {
				return $this->json($exception->getMessage(), Response::HTTP_BAD_GATEWAY);
			}
		}
		
		#[Route('/remove/{id}', methods: ['Delete']), OA\Delete, IsGranted("ROLE_ADMIN")]
		public function removeUser(int $id): JsonResponse
		{
			try {
				$user = $this->userRepository->find($id);
				
				if (!$user) {
					return $this->json(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
				}
				
				$this->entityManager->remove($user);
				$this->entityManager->flush();
				
				return $this->json(['message' => 'User and related positions removed successfully'], Response::HTTP_OK);
			} catch (Exception $exception) {
				return $this->json($exception->getMessage(), Response::HTTP_BAD_GATEWAY);
			}
		}
		
		#[Route('/user/{id}', methods: ['Get']), OA\Get]
		public function getUserById(int $id): JsonResponse
		{
			try {
				$user = $this->userRepository->find($id);
				
				if (!$user) {
					return $this->json(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
				}
				
				return $this->json($user, Response::HTTP_OK);
			} catch (Exception $exception) {
				return $this->json($exception->getMessage(), Response::HTTP_BAD_GATEWAY);
			}
		}
		
		#[Route('/get_email', methods: ['Post']), OA\Post]
		public function getUserByMail(#[MapRequestPayload(acceptFormat: 'json')] GetUserMailDTO $data): JsonResponse
		{
			try {
				$user = $this->userRepository->findOneBy([
					"email" =>  $data->email
				]);
				
				if (!$user) {
					return $this->json(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
				}
				
				return $this->json($user, Response::HTTP_OK);
			} catch (Exception $exception) {
				return $this->json($exception->getMessage(), Response::HTTP_BAD_GATEWAY);
			}
		}
	}
