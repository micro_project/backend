<?php

namespace App\Message;

final class SendEmail
{
    /*
     * Add whatever properties and methods you need
     * to hold the data for this message class.
     */

     private string $recipient;
     private string $subject;
     private string $body;

     public function __construct(string $recipient,string $subject,string $body)
     {
         $this->recipient = $recipient;
         $this->subject = $subject;
         $this->body = $body;
     }

  public function getRecipient(): string
  {
    return $this->recipient;
  }

  public function getSubject(): string
  {
    return $this->subject;
  }

  public function getBody(): string
  {
    return $this->body;
  }

}
