<?php
	
	namespace App\DataFixtures;
	
	use App\Entity\Positions;
	use Doctrine\Bundle\FixturesBundle\Fixture;
	use Doctrine\Persistence\ObjectManager;
	
	class PositionFixtures extends Fixture
	{
		public function load(ObjectManager $manager): void
		{
			$data = [
				[
					"name" => "Tester",
					"position" => [
						[
							"name" => "systemy testujące",
							"inputType" => "text",
							"inputValue" => null,
						],
						[
							"name" => "systemy raportowe",
							"inputType" => "text",
							"inputValue" => null,
						],
						[
							"name" => "zna selenium",
							"inputType" => "checkbox",
							"inputValue" => false,
						],
					],
				],
				[
					"name" => "Developer",
					"position" => [
						[
							"name" => "Środowiska ide",
							"inputType" => "text",
							"inputValue" => null,
						],
						[
							"name" => "Języki programowania",
							"inputType" => "text",
							"inputValue" => null,
						],
						[
							"name" => "zna mysql",
							"inputType" => "checkbox",
							"inputValue" => false,
						],
					],
				],
				[
					"name" => "Project manager",
					"position" => [
						[
							"name" => "metodologie prowadzenia projektów",
							"inputType" => "text",
							"inputValue" => null,
						],
						[
							"name" => "systemy raportowe",
							"inputType" => "text",
							"inputValue" => null,
						],
						[
							"name" => "zna scrum",
							"inputType" => "checkbox",
							"inputValue" => false,
						],
					],
				],
			];
			
			for ($i = 0; $i < count($data); $i++) {
				$position = new Positions();
				$position->setName($data[$i]['name']);
				$position->setSubPosition($data[$i]['position']);
				$manager->persist($position);
			}
			
			$manager->flush();
		}
	}
