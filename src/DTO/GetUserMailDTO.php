<?php
	
	namespace App\DTO;
	
	use Symfony\Component\Validator\Constraints as Assert;
	
	final class GetUserMailDTO
	{
		#[Assert\NotBlank(message: "fill this field"), Assert\Email(message: "This Email is not valid "), Assert\Type("string")]
		public string $email;
	}