<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

final class UpdateUserDTO
{
  #[Assert\NotBlank(message: "fill this field"), Assert\Type("string")]
  public string $name;

  #[Assert\NotBlank, Assert\Type("string")]
  public string $surname;

  #[Assert\Type("string")]
  public ?string $description = null;
	
	#[Assert\NotBlank(message: "fill this field"), Assert\Type("string"), Assert\Choice(
		choices: ['Tester', 'Developer', 'Project manager'],
		message: 'Choose position',
	)]
	public string $positionName = "Tester";
	
  #[Assert\NotBlank(message: "fill this field"), Assert\Type("array")]
  public array $answers =  [
	  [
		  "name" => 	"Potwierdzam",
		  "inputType" => "text",
	  ],
	  [
		  "name" => 	"Potwierdzam",
		  "inputType" => "text",
	  ],
	  [
		  "name" => 	false,
		  "inputType" => "checkbox",
	  ],
  ];
}