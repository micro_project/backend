<?php

namespace App\MessageHandler;

use App\Message\SendEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Mime\Email;

#[AsMessageHandler]
final class SendEmailHandler
{
  private MailerInterface $mailer;

  public function __construct(MailerInterface $mailer)
  {
    $this->mailer = $mailer;
  }

  public function __invoke(SendEmail $message): void
  {
    $email = (new Email())
      ->from('hello@example.com')
      ->to($message->getRecipient())
      ->subject($message->getSubject())
      ->html($message->getBody());

    try {
      $this->mailer->send($email);
    } catch (TransportExceptionInterface $e) {
     dd($e->getMessage());
    }
  }
}
