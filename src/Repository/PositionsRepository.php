<?php
	
	namespace App\Repository;
	
	use App\Entity\Positions;
  use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
  use Doctrine\Persistence\ManagerRegistry;
	
	/**
	 * @extends ServiceEntityRepository<Positions>
	 *
	 * @method Positions|null find($id, $lockMode = null, $lockVersion = null)
	 * @method Positions|null findOneBy(array $criteria, array $orderBy = null)
	 * @method Positions[]    findAll()
	 * @method Positions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
	 */
	class PositionsRepository extends ServiceEntityRepository
	{
		public function __construct(ManagerRegistry $registry)
		{
			parent::__construct($registry, Positions::class);
		}
		
		public function findAllPositionsNameWithSubPositions(): array
		{
			$queryBuilder = $this->createQueryBuilder('u');
			
			$sql = $queryBuilder
				->select('u.name, u.subPosition')
				->getQuery();
			
			return $sql
				->getArrayResult();
		}
	}
