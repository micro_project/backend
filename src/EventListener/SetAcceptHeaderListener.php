<?php
	
	namespace App\EventListener;
	
	use Symfony\Component\HttpKernel\Event\RequestEvent;
	
	class SetAcceptHeaderListener
	{
		public function onKernelRequest(RequestEvent $event): void
    {
			$request = $event->getRequest();
			
			if (!$request->headers->has('Accept') || $request->headers->get('Accept') === '*/*') {
				$request->headers->set('Accept', 'application/json');
			}
		}
	}